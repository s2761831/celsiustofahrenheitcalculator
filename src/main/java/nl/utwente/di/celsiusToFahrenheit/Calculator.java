package nl.utwente.di.celsiusToFahrenheit;

import java.util.HashMap;
import java.util.Map;

public class Calculator {

    public double getFahrenheit(String celsius) {
        return Double.parseDouble(celsius) * 1.8 + 32.0;
    }
}
